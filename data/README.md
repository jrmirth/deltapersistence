# Alkane Paper Data README

This folder contains the data files used to produce the figures in
`AlkaneFirstPaper.tex`.
This is a subset of the data provided at our [public data repository],
formatted so as to be compatible with our persistent homology codebase.
* OPLS-AA data are in `.npy` format, as these data consist of n-dimensional
  arrays. While not human-readable, this format is can be opened by python with
  the command `numpy.load()`.
* OPLS-UA data consist of two files per molecule, in a plain text format (here
  appended with `.dat`). The "angles" files contain the angular coordinates as
  two-dimensional, n*d arrays, while the "energy" files contain the energy
  values at those coordinates in one-dimensional, n-element arrays. These can
  be read with `numpy.loadtxt()`.

It is possible to recompute the sublevelset persistence for any of these data
sets. Assuming that the `deltapersistence` module is installed so that
`persistence_script.py` is in `$PATH` (alternatively, if the `deltapersistence`
repository is cloned to your system, and you replace `persistence_script.py`
with the entire path to that file) then the persistence can be computed as
follows:
* For OPLS-AA (`.npy` format) run, e.g., `python persistence_script.py -t mesh
  -f pentane_aa_averaged.npy -pd 11 --bars`
* For OPLA-UA (`.dat` format) run, e.g., `python persistence_script.py -t
  sample -f pentane_ua_angles_ds1000.dat -e pentane_ua_energy_ds1000.dat
  -pd 11 --bars`
* For analytic barcodes (not requiring any data file) run, e.g., `python
  persistence_script.py --carbons 5 -pd 11 --bars`
The complete set of options, including details for how to set titles,
filenames, et cetera, can be seen by running `python persistence_script.py
--help`.

All of the computational results from these files are found in the `results`
subfolder.
